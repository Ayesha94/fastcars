package com.example.mac.fastcarscustomers.DAOs

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.mac.fastcarscustomers.Entities.User

@Dao
interface UserDao {

    @Query("SELECT * FROM users")
    fun getUser(): LiveData<User>

    @Query("SELECT * FROM users")
    fun getCurrentUser(): User

    @Insert(onConflict = OnConflictStrategy.ROLLBACK)
    fun insertUser(user: User)
}