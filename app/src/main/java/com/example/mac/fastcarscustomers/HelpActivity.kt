package com.example.mac.fastcarscustomers

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_help.*

class HelpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)

        configureLayout()
        configureData()
    }

    fun configureLayout() {
        supportActionBar?.setTitle(R.string.title_activity_help)
    }

    fun configureData() {
        tvHelp.setText(R.string.help_content)
    }
}
