package com.example.mac.fastcarscustomers

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.example.mac.fastcarscustomers.Interface.OnLocationChangedListener
import com.example.mac.fastcarscustomers.Interface.OnRouteParseListener
import com.example.mac.fastcarscustomers.LocationManager.CustomLocationManager
import com.example.mac.fastcarscustomers.RouteParser.RouteParser
import com.example.mac.fastcarscustomers.ViewModel.UserViewModel
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.android.gms.maps.*
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*
import kotlinx.android.synthetic.main.nav_header_home.view.*
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.content_home.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory



class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, GoogleMap.OnMarkerDragListener, View.OnClickListener, OnRouteParseListener, OnLocationChangedListener {
    lateinit var viewModel: UserViewModel
    lateinit var map: GoogleMap
    lateinit var destinationMarker: Marker
    lateinit var coordinates: LatLng
    lateinit var polyline: Polyline
    lateinit  var routeParser: RouteParser
    lateinit var currentLocation: LatLng
    lateinit var routeLatLng: ArrayList<LatLng>
    private var currentLocationMarker: Marker? = null
    private var LOCATION_REQUEST_CODE = 100
    private var PLACE_AUTOCOMPLETE_REQUEST_CODE = 200
    private val ZOOM_LEVEL = 15f
    lateinit var customLocationManager: CustomLocationManager
    private var latlngBounds =  LatLngBounds.Builder()
    private  var isPathDrawn = false
    private var mapView: View? = null
    private var isPermisionGranted = false
    private var isMapLoaded = false
    private var driversLocations = ArrayList<LatLng>()
    var driversMarkers = ArrayList<Marker>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setSupportActionBar(toolbar)
        configureLayout()
        configureData()
        enableMyLocationIfPermitted(null)
    }

    private fun configureLayout() {
        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)

        viewModel = UserViewModel(application)
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.fMap) as SupportMapFragment
        mapFragment.getMapAsync(this)
        mapView = mapFragment.getView()

        routeParser = RouteParser(this)
        customLocationManager = CustomLocationManager(this, this)
        routeLatLng = ArrayList<LatLng>()
        tvPlacesSuggestion.setOnClickListener(this)
    }

    private fun configureData() {
        supportActionBar?.setTitle(R.string.title_activity_home)
        nav_view.getHeaderView(0).tvUserName.text = viewModel.getCurrentUser().firstName + " " + viewModel.getCurrentUser().lastName
        nav_view.getHeaderView(0).tvUserEmail.text = viewModel.getCurrentUser().email
        coordinates = LatLng(31.5204, 74.3587)
    }

    private fun enableMyLocationIfPermitted(googleMap: GoogleMap?){
        val permission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf<String>(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_REQUEST_CODE)
        } else {
            isPermisionGranted = true
            if (googleMap != null) {
                map.isMyLocationEnabled = true
            }
            val loc = customLocationManager.getCurrentLocation()
            if (loc != null) {
                currentLocation = LatLng(loc.latitude, loc.longitude)
                drawMarker(loc)
                calculateDriverLatlng()
            }

        }
    }

    private fun calculateDriverLatlng() {
        driversLocations.clear()
        driversLocations = customLocationManager.getDriversLocation(currentLocation)
        showDriversIcon()

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            coordinates = LatLng(38.9637, 35.2433)
            Toast.makeText(this, getString(R.string.location_permission_not_granted), Toast.LENGTH_SHORT).show()
        } else {
            enableMyLocationIfPermitted(map)
        }
    }

    private fun drawMarker(location: Location) {
        if (!isPathDrawn && isMapLoaded) {
            coordinates = LatLng(location.latitude, location.longitude)
            if (currentLocationMarker != null) {
                currentLocationMarker?.setPosition(coordinates)
            } else {
                val markerOptions = MarkerOptions()
                markerOptions.position(coordinates)
                markerOptions.title("Current Location")
                currentLocationMarker = map.addMarker(markerOptions)
            }
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinates, ZOOM_LEVEL))
        }
    }

    private fun showRoute(from: LatLng, to: LatLng) {
        routeLatLng.clear()
        if (isPathDrawn) {
            polyline.remove()
        }
        routeParser.getURL(from, to)
    }

    private fun showDriversIcon() {
        if (driversMarkers.size > 0) {
            val size = driversLocations.size
            var index = 0
            var latlng: LatLng
            while(index < size) {
                latlng = driversLocations.elementAt(index)
                driversMarkers.elementAt(index).position = latlng
                driversMarkers.elementAt(index).title = "Driver $index"
                index++
            }
        } else {
            val icon = BitmapDescriptorFactory.fromResource(R.drawable.car)
            for (driver in driversLocations) {
                val m = MarkerOptions()
                m.position(driver)
                m.icon(icon)
                val mr = map.addMarker(m)
                driversMarkers.add(mr)
            }
        }
        print("Drivers markers Size: ${driversMarkers.size}")
    }

    override fun onRouteParsed(latlngs: ArrayList<LatLng>) {
        var polylineOptions = PolylineOptions().color(R.color.colorAccent).width(15F)

        routeLatLng.addAll(latlngs)
        for (latlng in routeLatLng) {
            polylineOptions.add(latlng)
            latlngBounds.include(latlng)
        }
        isPathDrawn = true
        val bounds = latlngBounds.build()
        runOnUiThread {
            polyline = map.addPolyline(polylineOptions)
            map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 20))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                coordinates = PlaceAutocomplete.getPlace(this, data).latLng
                var markerOptions = MarkerOptions()
                markerOptions.draggable(true)
                markerOptions.position(coordinates)
                markerOptions.title("Destination")
                if (isPathDrawn) {
                    destinationMarker.setPosition(coordinates)
                } else {
                    destinationMarker = map.addMarker(markerOptions)
                }
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinates, ZOOM_LEVEL))
                showRoute(currentLocation, coordinates)
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                print("There is an error.")
            } else if (resultCode == Activity.RESULT_CANCELED) {
                print("The use canceled the operation")
            }
        }
    }

    override fun onLocationChanged(location: Location) {
        currentLocation = LatLng(location.latitude, location.longitude)
        drawMarker(location)
        calculateDriverLatlng()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> {

            }
            R.id.nav_help -> {
                val intent = Intent(this, HelpActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_about_us -> {
                val intent = Intent(this, AboutUsActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_privacy_policy -> {
                val intent = Intent(this, PrivacyPolicyActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.mapType = GoogleMap.MAP_TYPE_TERRAIN
        map.setOnMarkerDragListener(this)
        map.isBuildingsEnabled = true
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            map.isMyLocationEnabled = true
        }
        isMapLoaded = true
//        val v = mapView?.findViewById<View>(Integer.parseInt("1"))
//        if (map != null && v != null) {
//            val locationButton = (v.getParent() as View).findViewById<View>(Integer.parseInt("2"))
//            val rlp = locationButton.getLayoutParams()
//            print(rlp)
//        }
    }

    override fun onMarkerDrag(p0: Marker?) {
    }

    override fun onMarkerDragStart(p0: Marker?) {
    }

    override fun onMarkerDragEnd(p0: Marker?) {
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(p0?.getPosition(), ZOOM_LEVEL))
        //Show confirm destination Alert. On click of OK show route and set destinationMarker position. On cancel move marker to previous position or as per requirement.
        val destinationLatLng = LatLng(p0?.position!!.latitude, p0.position.longitude)
        showRoute(currentLocation, destinationLatLng)
    }

    override fun onClick(v: View?) {
        when(v?.getId()){
            R.id.tvPlacesSuggestion -> {
                try {
                    val filter = AutocompleteFilter.Builder()
                            .setCountry("PK")
                            .build()
                    val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(filter)
                            .build(this)
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE)
                } catch (e: GooglePlayServicesRepairableException) {

                } catch (e: GooglePlayServicesNotAvailableException) {

                }
            }
        }
    }

    override fun onStop() {
        super.onStop()

        viewModel.getUser().removeObservers(this)
    }
}
