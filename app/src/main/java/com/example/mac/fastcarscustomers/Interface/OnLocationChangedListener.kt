package com.example.mac.fastcarscustomers.Interface

import android.location.Location

interface OnLocationChangedListener {
    fun onLocationChanged(location: Location)
}