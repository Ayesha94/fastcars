package com.example.mac.fastcarscustomers

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.View
import com.example.mac.fastcarscustomers.Entities.User
import com.example.mac.fastcarscustomers.ViewModel.UserViewModel
import com.facebook.accountkit.*
import com.facebook.accountkit.ui.AccountKitActivity
import com.facebook.accountkit.ui.AccountKitConfiguration
import com.facebook.accountkit.ui.LoginType
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    val APP_REQUEST_CODE = 99
    lateinit var viewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        checkAccessToken()
        configureData()
    }

    fun configureData() {
        viewModel = UserViewModel(application)
        btnSignUp.setOnClickListener(this)
    }

    private fun checkAccessToken() {
        val accessToken = AccountKit.getCurrentAccessToken()

        if (accessToken != null) {
            getCurrentUserInfo()
        } else {
            phoneLogin()
        }
    }

    private fun getCurrentUserInfo() {
        AccountKit.getCurrentAccount(object : AccountKitCallback<Account> {
            override fun onSuccess(account: Account) {

                val phoneNumber = account.phoneNumber
                if (phoneNumber != null) {
                    System.out.println("PhoneNumber: $phoneNumber")
                    etPhoneNumber.text = Editable.Factory.getInstance().newEditable(phoneNumber.toString())
                }
            }

            override fun onError(error: AccountKitError) {
                Log.d("Error", error.toString())
            }
        })

    }

    fun phoneLogin() {
        val intent = Intent(this, AccountKitActivity::class.java)
        val configurationBuilder = AccountKitConfiguration.AccountKitConfigurationBuilder(
                LoginType.PHONE,
                AccountKitActivity.ResponseType.TOKEN)
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build())
        startActivityForResult(intent, APP_REQUEST_CODE)
    }

    override fun onClick(v: View?) {
        when (v) {
            btnSignUp -> {
                var user = User(etFirstName.text.toString(), etLastName.text.toString(), etUsername.text.toString(), etEmail.text.toString(), etPhoneNumber.text.toString(), "5")
                viewModel.insertUser(user)
                onUserInserted()
            }
        }
    }

    fun onUserInserted() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == APP_REQUEST_CODE) {
            val loginResult: AccountKitLoginResult = data.getParcelableExtra<AccountKitLoginResult>(AccountKitLoginResult.RESULT_KEY)
            val errorMessage: String
            if (loginResult.error != null) {
                errorMessage = loginResult.error!!.errorType.message
                System.out.println(errorMessage)
            } else if (loginResult.wasCancelled()) {

            } else {
                if (loginResult.accessToken != null) {
                    getCurrentUserInfo()
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()

        viewModel.getUser().removeObservers(this)
    }
}
