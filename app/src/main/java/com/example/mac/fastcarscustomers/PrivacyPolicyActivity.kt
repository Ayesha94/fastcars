package com.example.mac.fastcarscustomers

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_privacy_policy.*

class PrivacyPolicyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)

        configureLayout()
        configureData()
    }

    fun configureLayout() {
        supportActionBar?.setTitle(R.string.title_activity_privacy_policy)
    }

    fun configureData() {
        tvPrivacyPolicy?.setText(R.string.privacy_content)
    }
}
