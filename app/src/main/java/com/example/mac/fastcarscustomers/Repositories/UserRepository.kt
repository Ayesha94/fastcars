package com.example.mac.fastcarscustomers.Repositories

import android.app.Application
import android.arch.lifecycle.LiveData
import com.example.mac.fastcarscustomers.DAOs.UserDao
import com.example.mac.fastcarscustomers.Database.AppDatabase
import com.example.mac.fastcarscustomers.Entities.User

class UserRepository internal constructor(application: Application) {

    private val userDao: UserDao

    init {
        val db = AppDatabase.getDatabase(application)
        userDao = db.userDao()
    }

    fun getUser() : LiveData<User> {
        return userDao.getUser()
    }

    fun getCurrentUser(): User {
        return userDao.getCurrentUser()
    }

    fun insertUser(newUser: User) {
        userDao.insertUser(newUser)
    }
}