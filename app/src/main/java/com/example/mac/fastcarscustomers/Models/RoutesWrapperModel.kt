package com.example.mac.fastcarscustomers.Models

import android.arch.persistence.room.Ignore

data class RoutesWrapperModel(
        @Ignore val geocoded_waypoints: List<Geocoded_waypoints>,
        val routes: List<Routes>,
        val status: String
)

data class Geocoded_waypoints(
        val geocoder_status: String,
        val place_id: String,
        val types: List<String>
)

data class Routes(
        @Ignore val bounds: Bounds,
        @Ignore val copyrights: String,
        val legs: List<Legs>,
        @Ignore val overview_polyline: Overview_polyline,
        @Ignore val summary: String,
        @Ignore val warnings: List<Any>,
        @Ignore val waypoint_order: List<Any>
)

data class Legs(
        @Ignore val distance: Distance,
        @Ignore val duration: Duration,
        @Ignore val end_address: String,
        @Ignore val end_location: End_location,
        @Ignore val start_address: String,
        @Ignore val start_location: Start_location,
        val steps: List<Steps>,
        @Ignore val traffic_speed_entry: List<Any>,
        @Ignore val via_waypoint: List<Any>
)

data class Steps(
        val distance: Distance,
        val duration: Duration,
        val end_location: End_location,
        val html_instructions: String,
        val maneuver: String? = null,
        val polyline: Polyline,
        val start_location: Start_location,
        val travel_mode: String
)

data class Bounds(
        val northeast: Northeast,
        val southwest: Southwest
)

data class Distance(
        val text: String,
        val value: Number
)

data class Duration(
        val text: String,
        val value: Number
)

data class End_location(
        val lat: Number,
        val lng: Number
)

data class Northeast(
        val lat: Number,
        val lng: Number
)

data class Overview_polyline(
        val points: String
)

data class Polyline(
        val points: String
)

data class Southwest(
        val lat: Number,
        val lng: Number
)

data class Start_location(
        val lat: Number,
        val lng: Number
)
