package com.example.mac.fastcarscustomers.ViewModel

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.example.mac.fastcarscustomers.Entities.User
import com.example.mac.fastcarscustomers.Repositories.UserRepository

class UserViewModel : ViewModel {
    private var userRepository: UserRepository

    constructor(application: Application) : super() {
        userRepository = UserRepository(application)
    }

    fun getUser(): LiveData<User> {
        return userRepository.getUser()
    }

    fun getCurrentUser(): User {
        return userRepository.getCurrentUser()
    }

    fun insertUser(user: User) {
        userRepository.insertUser(user)
    }
}