package com.example.mac.fastcarscustomers.Entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "users")
class User(@ColumnInfo(name = "first_name") var firstName: String,
                 @ColumnInfo(name = "last_name") var lastName: String,
                 @ColumnInfo(name = "user_name") var userName: String,
                 @ColumnInfo(name = "email") var email: String,
                 @ColumnInfo(name = "phone_number") @PrimaryKey var phoneNumber: String,
                 @ColumnInfo(name = "rating") var ratings: String)