package com.example.mac.fastcarscustomers.LocationManager

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.widget.Toast
import com.example.mac.fastcarscustomers.Interface.OnLocationChangedListener
import com.google.android.gms.maps.model.LatLng
import java.util.*
import kotlin.collections.ArrayList

class CustomLocationManager (val onLocationChangedListener: OnLocationChangedListener, _context: Context) : LocationListener {
    private var locationManager: LocationManager
    private val context: Context
    private val LOCATION_UPDATE_MIN_DISTANCE: Float = 10f
    private val LOCATION_UPDATE_MIN_TIME: Long = 5000

    init {
        this.context = _context
        locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    fun getCurrentLocation() : Location? {
        val isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        val isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

        var location: Location? = null

        if (!(isGPSEnabled || isNetworkEnabled)) {
            Toast.makeText(context, "Error location provider", Toast.LENGTH_SHORT).show()
        } else {
            if (isNetworkEnabled) {

                if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return null
                }
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, this)
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
            }
            if (isGPSEnabled) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, this)
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            }
        }
        return location
    }

    override fun onLocationChanged(location: Location) {
        onLocationChangedListener.onLocationChanged(location)
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

    }

    override fun onProviderEnabled(provider: String?) {

    }

    override fun onProviderDisabled(provider: String?) {

    }

    fun getDriversLocation(currentLatLng: LatLng) : ArrayList<LatLng> {
        var driversLatLng = ArrayList<LatLng>()
        var i = 10
        while (i > 0) {
            val latlng = getLocation(currentLatLng.latitude, currentLatLng.longitude, 1000)
            driversLatLng.add(latlng)
            i--
        }
        return driversLatLng
    }

    fun getLocation(lon: Double, lat: Double, radius: Int): LatLng {
        val x0 = lat
        val y0 = lon

        val random = Random()

        // Convert radius from meters to degrees.
        val radiusInDegrees = radius / 111320f

        // Get a random distance and a random angle.
        val u = random.nextDouble()
        val v = random.nextDouble()
        val w = radiusInDegrees * Math.sqrt(u)
        val t = 2.0 * Math.PI * v
        // Get the x and y delta values.
        val x = w * Math.cos(t)
        val y = w * Math.sin(t)

        // Compensate the x value.
        val new_x = x / Math.cos(Math.toRadians(y0))

        val foundLatitude: Double
        val foundLongitude: Double

        foundLatitude = y0 + y
        foundLongitude = x0 + new_x

        return LatLng(foundLatitude, foundLongitude)

    }
}