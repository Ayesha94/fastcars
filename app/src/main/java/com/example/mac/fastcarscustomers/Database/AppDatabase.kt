package com.example.mac.fastcarscustomers.Database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.mac.fastcarscustomers.Entities.User
import com.example.mac.fastcarscustomers.DAOs.UserDao
import android.arch.persistence.room.Room
import android.content.Context


@Database(entities = [(User::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        lateinit var INSTANCE: AppDatabase

        fun getDatabase(context: Context): AppDatabase {
            synchronized(AppDatabase::class.java) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        AppDatabase::class.java, "fast-cars-customers-db")
                        .allowMainThreadQueries()
                        .build()
            }
            return INSTANCE
        }
    }

    abstract fun userDao() : UserDao
}