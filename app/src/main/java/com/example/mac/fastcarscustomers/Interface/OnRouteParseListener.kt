package com.example.mac.fastcarscustomers.Interface

import com.google.android.gms.maps.model.LatLng

interface OnRouteParseListener {
    fun onRouteParsed(latlng: ArrayList<LatLng>)
}