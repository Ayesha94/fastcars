package com.example.mac.fastcarscustomers.RouteParser

import com.beust.klaxon.Klaxon
import com.example.mac.fastcarscustomers.Interface.OnRouteParseListener
import com.example.mac.fastcarscustomers.Models.RoutesWrapperModel
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.experimental.async
import java.net.URL

class RouteParser (val routeParserListener:  OnRouteParseListener) {

    private var routeLatLng = ArrayList<LatLng>()

    fun getURL(from : LatLng, to : LatLng) {
        routeLatLng.clear()
        val origin = "origin=" + from.latitude + "," + from.longitude
        val dest = "destination=" + to.latitude + "," + to.longitude
        val sensor = "sensor=false"
        val params = "$origin&$dest&$sensor"
        getPolyine("https://maps.googleapis.com/maps/api/directions/json?$params")
    }

    fun getPolyine(url: String) {
        async {
            var response = URL(url).readText()
            response = response.replace(" ", "")
            response = response.replace("\n", "")

            val routeParserResponse = Klaxon().parse<RoutesWrapperModel>(json = response)
            val polypts = routeParserResponse!!.routes[0].legs[0].steps
            val polylines = polypts.map { it.polyline.points }
            for (polyline in polylines) {
                val latLngs = decodePoly(polyline)
                for (latLng in latLngs) {
                    routeLatLng.add(latLng)
                }
            }
            routeParserListener.onRouteParsed(routeLatLng)
        }
    }

    private fun decodePoly(encoded: String): List<LatLng> {
        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val p = LatLng(lat.toDouble() / 1E5,
                    lng.toDouble() / 1E5)
            poly.add(p)
        }

        return poly
    }
}