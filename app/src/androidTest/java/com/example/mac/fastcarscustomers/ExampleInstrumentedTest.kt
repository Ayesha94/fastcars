package com.example.mac.fastcarscustomers

import android.arch.persistence.room.Room
import android.content.Context
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import android.util.Log
import com.example.mac.fastcarscustomers.DAOs.UserDao
import com.example.mac.fastcarscustomers.Database.AppDatabase
import org.junit.*

import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ListCategoryDaoTest {


    private lateinit var database: AppDatabase
    private lateinit var userDao: UserDao

    @Before
    fun setup() {
        val context: Context = InstrumentationRegistry.getTargetContext()
        try {
            database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java)
                    .allowMainThreadQueries().build()
        } catch (e: Exception) {
            Log.i("test", e.message)
        }
        userDao = database.userDao()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun testAddingAndRetrievingData() {
//        // 1
//        val preInsertRetrievedCategories = userDao.getAll()
//
//        // 2
//        val user = Users("Ayesha", "Saeed", "ayesha94", "ayesha.saeed@clustox.com", "+923229815650", "5")
//        userDao.insertAll(user)
//
//        //3
//        val postInsertRetrievedCategories = userDao.getAll()
//        val sizeDifference = postInsertRetrievedCategories.size - preInsertRetrievedCategories.size
//        Assert.assertEquals(1, sizeDifference)
//        val retrievedCategory = postInsertRetrievedCategories.last()
//        Assert.assertEquals("Ayesha", retrievedCategory.firstName)
    }
}