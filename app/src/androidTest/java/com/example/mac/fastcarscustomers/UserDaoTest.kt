package com.example.mac.fastcarscustomers

import android.arch.persistence.room.Room
import android.content.Context
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import android.util.Log
import com.example.mac.fastcarscustomers.DAOs.UserDao
import com.example.mac.fastcarscustomers.Database.AppDatabase
import com.example.mac.fastcarscustomers.Entities.User
import org.junit.*
import org.junit.rules.TestRule
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UserDaoTest {

//    @Rule
//    @JvmField
//    val rule: TestRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var userDao: UserDao

    @Before
    fun setup() {
        val context: Context = InstrumentationRegistry.getTargetContext()
        try {
            database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).allowMainThreadQueries().build()
        } catch (e: Exception) {
            Log.i("Test", e.message)
        }
        userDao = database.userDao()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun testAddingAndRetrievingData() {
        val preInsertRetrievedUser = userDao.getUser()

        val user = User("Ayesha", "Saeed", "ayesha94", "ayesha.saeed@clustox.com", "+923229815650", "5")
        userDao.insertUser(user)

        val postInsertRetrievedUser = userDao.getUser()
        val retrievedUser = postInsertRetrievedUser
        Assert.assertEquals("ayesha94", retrievedUser.userName)
    }
}